"use strict";
exports.__esModule = true;
exports.dbClient = void 0;
var pg_1 = require("pg");
exports.dbClient = new pg_1.Client({
    user: 'postgres',
    password: process.env.DBPASSWORD,
    database: 'project0db',
    port: 5432,
    host: '35.236.213.14'
});
exports.dbClient.connect();
