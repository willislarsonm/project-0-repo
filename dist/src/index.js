"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var express_1 = __importDefault(require("express"));
var account_service_impl_1 = require("./service/account-service-impl");
var client_service_impl_1 = require("./service/client-service-impl");
var app = express_1["default"]();
var clientService = new client_service_impl_1.ClientServiceImpl();
var accountService = new account_service_impl_1.AccountServiceImpl();
app.use(express_1["default"].json());
var port = 3000;
app.listen(port, function () {
    console.log("Server started at HTTP://127.0.0.1:" + port);
});
app.get("/", function (req, res) {
    res.send("server up");
});
app.post("/clients", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var newClient;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                newClient = req.body;
                return [4 /*yield*/, clientService.registerClient(newClient)];
            case 1:
                newClient = _a.sent();
                res.status(201);
                res.send(newClient);
                return [2 /*return*/];
        }
    });
}); });
app.get("/clients", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var clients, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, clientService.retrieveAllClients()];
            case 1:
                clients = _a.sent();
                res.status(200);
                res.send(clients);
                return [3 /*break*/, 3];
            case 2:
                error_1 = _a.sent();
                res.status(404);
                res.send();
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.get("/clients/:clientId", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var clientId, client, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                clientId = parseInt(req.params.clientId);
                return [4 /*yield*/, clientService.retrieveClient(clientId)];
            case 1:
                client = _a.sent();
                res.status(200);
                res.send(client);
                return [3 /*break*/, 3];
            case 2:
                error_2 = _a.sent();
                res.status(404);
                res.send(error_2.message);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.put("/clients/:clientId", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var updateClient, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                updateClient = req.body;
                updateClient.clientId = parseInt(req.params.clientId);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, clientService.updateClient(updateClient)];
            case 2:
                updateClient = _a.sent();
                res.status(200);
                res.send(updateClient);
                return [3 /*break*/, 4];
            case 3:
                error_3 = _a.sent();
                res.status(404);
                res.send();
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
app["delete"]("/clients/:clientId", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var clientId, deleted, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                clientId = parseInt(req.params.clientId);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, clientService.deleteClientById(clientId)];
            case 2:
                deleted = _a.sent();
                res.status(205);
                res.send(deleted);
                return [3 /*break*/, 4];
            case 3:
                error_4 = _a.sent();
                if (error_4.message === "Error: Client with id " + clientId + " not found") {
                    //resource not found
                    res.status(404);
                    res.send(error_4.message);
                }
                else {
                    //client has attached accounts
                    res.status(403);
                    res.send(error_4.message);
                }
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
app.post("/clients/:clientId/accounts", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var clientId, account, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                clientId = parseInt(req.params.clientId);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                account = req.body;
                account.c_id = clientId;
                return [4 /*yield*/, accountService.registerAccount(account)];
            case 2:
                account = _a.sent();
                res.status(201);
                res.send(account);
                return [3 /*break*/, 4];
            case 3:
                error_5 = _a.sent();
                res.status(404);
                res.send(error_5.message);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
app.get("/clients/:clientId/accounts", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var clientId, clientCheck, accounts, error_6;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                clientId = parseInt(req.params.clientId);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 4, , 5]);
                return [4 /*yield*/, clientService.retrieveClient(clientId)];
            case 2:
                clientCheck = _a.sent();
                return [4 /*yield*/, accountService.retrieveClientAccounts(clientId)];
            case 3:
                accounts = _a.sent();
                res.status(200);
                res.send(accounts);
                return [3 /*break*/, 5];
            case 4:
                error_6 = _a.sent();
                res.status(404);
                res.send(error_6.message);
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); });
app.get("/accounts", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var greaterThan, lessThan, accounts;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!(req.query.amountLessThan && req.query.amountGreaterThan)) return [3 /*break*/, 2];
                greaterThan = parseInt(req.query.amountGreaterThan);
                lessThan = parseInt(req.query.amountLessThan);
                return [4 /*yield*/, accountService.retrieveAccountsInRange(greaterThan, lessThan)];
            case 1:
                accounts = _a.sent();
                res.status(200);
                res.send(accounts);
                return [3 /*break*/, 3];
            case 2:
                res.status(400);
                res.send("Error: amountLessThan and amountGreaterThan must have values");
                _a.label = 3;
            case 3: return [2 /*return*/];
        }
    });
}); });
app.get("/accounts/:accoundId", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var accountId, account, error_7;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                accountId = parseInt(req.params.accoundId);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, accountService.retrieveAccountById(accountId)];
            case 2:
                account = _a.sent();
                res.status(200);
                res.send(account);
                return [3 /*break*/, 4];
            case 3:
                error_7 = _a.sent();
                res.status(404);
                res.send(error_7.message);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
app.put("/accounts/:accountId", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var accountId, account, clientCheck, error_8;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                accountId = parseInt(req.params.accountId);
                account = req.body;
                account.accountId = accountId;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 4, , 5]);
                return [4 /*yield*/, clientService.retrieveClient(account.c_id)];
            case 2:
                clientCheck = _a.sent();
                return [4 /*yield*/, accountService.updateAccount(account)];
            case 3:
                account = _a.sent();
                res.status(200);
                res.send(account);
                return [3 /*break*/, 5];
            case 4:
                error_8 = _a.sent();
                res.status(404);
                res.send(error_8.message);
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); });
app["delete"]("/accounts/:accountId", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var accountId, deleted, error_9;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                accountId = parseInt(req.params.accountId);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, accountService.deleteAccount(accountId)];
            case 2:
                deleted = _a.sent();
                res.send(deleted);
                return [3 /*break*/, 4];
            case 3:
                error_9 = _a.sent();
                res.status(404);
                res.send(error_9.message);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
app.patch("/accounts/:accountId/deposit", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var accountId, amount, updateAccount, error_10;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                accountId = parseInt(req.params.accountId);
                amount = req.body.amount;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, accountService.depositAmount(accountId, amount)];
            case 2:
                updateAccount = _a.sent();
                res.status(200);
                res.send(updateAccount);
                return [3 /*break*/, 4];
            case 3:
                error_10 = _a.sent();
                if (error_10.message === "Error: Cannot Deposit a Negative Amount") {
                    res.status(422);
                }
                else {
                    res.status(404);
                }
                res.send(error_10.message);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
app.patch("/accounts/:accountId/withdraw", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var accountId, amount, updateAccount, error_11;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                accountId = parseInt(req.params.accountId);
                amount = req.body.amount;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, accountService.withdrawAmount(accountId, amount)];
            case 2:
                updateAccount = _a.sent();
                res.status(200);
                res.send(updateAccount);
                return [3 /*break*/, 4];
            case 3:
                error_11 = _a.sent();
                if (error_11.message === "Error: Cannot Withdraw a Negative Amount" ||
                    error_11.message === "Error: Insufficient Funds") {
                    res.status(422);
                }
                else {
                    res.status(404);
                }
                res.send(error_11.message);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
