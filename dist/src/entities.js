"use strict";
exports.__esModule = true;
exports.Account = exports.Client = void 0;
var Client = /** @class */ (function () {
    function Client(clientId, name) {
        this.clientId = clientId;
        this.name = name;
    }
    return Client;
}());
exports.Client = Client;
var Account = /** @class */ (function () {
    function Account(accountId, type, amount, c_id) {
        this.accountId = accountId;
        this.type = type;
        this.amount = amount;
        this.c_id = c_id;
    }
    return Account;
}());
exports.Account = Account;
