"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.ClientDAOImpl = void 0;
var connection_1 = require("../connection");
var entities_1 = require("../entities");
var errors_1 = require("../errors");
var ClientDAOImpl = /** @class */ (function () {
    function ClientDAOImpl() {
    }
    ClientDAOImpl.prototype.createClient = function (client) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = "insert into client (client_name) values ($1) returning client_id";
                        values = [client.name];
                        return [4 /*yield*/, connection_1.dbClient.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        client.clientId = result.rows[0].client_id;
                        return [2 /*return*/, client];
                }
            });
        });
    };
    ClientDAOImpl.prototype.getAllClients = function () {
        return __awaiter(this, void 0, void 0, function () {
            var sql, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = "select * from client";
                        return [4 /*yield*/, connection_1.dbClient.query(sql)];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, result.rows.map(function (c) { return new entities_1.Client(c.client_id, c.client_name); })];
                }
            });
        });
    };
    ClientDAOImpl.prototype.getClientById = function (clientId) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = "select * from client where client_id=$1";
                        values = [clientId];
                        return [4 /*yield*/, connection_1.dbClient.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        if (result.rowCount === 0) {
                            throw new errors_1.ResourceNotFoundError("Error: Client with id " + clientId + " not found");
                        }
                        return [2 /*return*/, result.rows.map(function (c) { return new entities_1.Client(c.client_id, c.client_name); })[0]];
                }
            });
        });
    };
    ClientDAOImpl.prototype.updateClient = function (client) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = 'update client set client_name=$1 where client_id=$2';
                        values = [client.name, client.clientId];
                        return [4 /*yield*/, connection_1.dbClient.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        if (result.rowCount === 0) {
                            throw new errors_1.ResourceNotFoundError("Error: Client with id " + client.clientId + " not found");
                        }
                        return [2 /*return*/, client];
                }
            });
        });
    };
    ClientDAOImpl.prototype.deleteClientById = function (clientId) {
        return __awaiter(this, void 0, void 0, function () {
            function checkClientForAccounts(clientId) {
                return __awaiter(this, void 0, void 0, function () {
                    var sql, values, result;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                sql = "select * from account where c_id = $1";
                                values = [clientId];
                                return [4 /*yield*/, connection_1.dbClient.query(sql, values)];
                            case 1:
                                result = _a.sent();
                                return [2 /*return*/, result.rowCount > 0];
                        }
                    });
                });
            }
            var sql, values, result, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = 'delete from client where client_id = $1';
                        values = [clientId];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 5]);
                        return [4 /*yield*/, connection_1.dbClient.query(sql, values)];
                    case 2:
                        result = _a.sent();
                        if (result.rowCount === 0) {
                            throw new errors_1.ResourceNotFoundError("Error: Client with id " + clientId + " not found");
                        }
                        return [2 /*return*/, true];
                    case 3:
                        error_1 = _a.sent();
                        return [4 /*yield*/, checkClientForAccounts(clientId)];
                    case 4:
                        //this solution isn't great, but works
                        if (_a.sent()) {
                            throw new errors_1.IllegalDeleteError("Error: Cannot Delete Client " + clientId + " as it has Unclosed Accounts");
                        }
                        else {
                            throw new errors_1.ResourceNotFoundError("Error: Client with id " + clientId + " not found");
                        }
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    return ClientDAOImpl;
}());
exports.ClientDAOImpl = ClientDAOImpl;
