"use strict";
exports.__esModule = true;
exports.InvalidInput = exports.InsufficientFundsError = exports.IllegalDeleteError = exports.ResourceNotFoundError = void 0;
var ResourceNotFoundError = /** @class */ (function () {
    function ResourceNotFoundError(message) {
        this.message = message;
    }
    return ResourceNotFoundError;
}());
exports.ResourceNotFoundError = ResourceNotFoundError;
var IllegalDeleteError = /** @class */ (function () {
    function IllegalDeleteError(message) {
        this.message = message;
    }
    return IllegalDeleteError;
}());
exports.IllegalDeleteError = IllegalDeleteError;
var InsufficientFundsError = /** @class */ (function () {
    function InsufficientFundsError(message) {
        this.message = message;
    }
    return InsufficientFundsError;
}());
exports.InsufficientFundsError = InsufficientFundsError;
var InvalidInput = /** @class */ (function () {
    function InvalidInput(message) {
        this.message = message;
    }
    return InvalidInput;
}());
exports.InvalidInput = InvalidInput;
