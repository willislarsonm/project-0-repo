"use strict";
exports.__esModule = true;
exports.ClientServiceImpl = void 0;
var client_dao_impl_1 = require("../daos/client-dao-impl");
var clientDAO = new client_dao_impl_1.ClientDAOImpl();
var ClientServiceImpl = /** @class */ (function () {
    function ClientServiceImpl() {
    }
    ClientServiceImpl.prototype.registerClient = function (client) {
        return clientDAO.createClient(client);
    };
    ClientServiceImpl.prototype.retrieveClient = function (clientId) {
        return clientDAO.getClientById(clientId);
    };
    ClientServiceImpl.prototype.retrieveAllClients = function () {
        return clientDAO.getAllClients();
    };
    ClientServiceImpl.prototype.updateClient = function (client) {
        return clientDAO.updateClient(client);
    };
    ClientServiceImpl.prototype.deleteClientById = function (clientId) {
        return clientDAO.deleteClientById(clientId);
    };
    return ClientServiceImpl;
}());
exports.ClientServiceImpl = ClientServiceImpl;
