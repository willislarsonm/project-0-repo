"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var connection_1 = require("../src/connection");
var account_dao_impl_1 = require("../src/daos/account-dao-impl");
var client_dao_impl_1 = require("../src/daos/client-dao-impl");
var entities_1 = require("../src/entities");
var accountDAO = new account_dao_impl_1.AccountDAOImpl();
var clientDAO = new client_dao_impl_1.ClientDAOImpl();
test("Test Create Account", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, account;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, "account test1");
                return [4 /*yield*/, clientDAO.createClient(client)];
            case 1:
                client = _a.sent();
                //check client was made correctly
                expect(client.clientId).not.toBe(0);
                account = new entities_1.Account(0, "Savings", 0, client.clientId);
                return [4 /*yield*/, accountDAO.createAccount(account)];
            case 2:
                account = _a.sent();
                //check account was made
                expect(account.accountId).not.toBe(0);
                return [2 /*return*/];
        }
    });
}); });
test("Test Get Client Accounts", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, account1, account2, account3, accounts;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, "account test2");
                return [4 /*yield*/, clientDAO.createClient(client)];
            case 1:
                client = _a.sent();
                //check client was made
                expect(client.clientId).not.toBe(0);
                account1 = new entities_1.Account(0, "Savings", 0, client.clientId);
                account2 = new entities_1.Account(0, "Savings", 0, client.clientId);
                account3 = new entities_1.Account(0, "Savings", 0, client.clientId);
                return [4 /*yield*/, accountDAO.createAccount(account1)];
            case 2:
                account1 = _a.sent();
                return [4 /*yield*/, accountDAO.createAccount(account2)];
            case 3:
                account2 = _a.sent();
                return [4 /*yield*/, accountDAO.createAccount(account3)];
            case 4:
                account3 = _a.sent();
                //check accounts were made
                expect(account1.accountId).not.toBe(0);
                expect(account2.accountId).not.toBe(0);
                expect(account3.accountId).not.toBe(0);
                return [4 /*yield*/, accountDAO.getAllClientsAccounts(client.clientId)];
            case 5:
                accounts = _a.sent();
                //check all 3 were added to the client and no more
                expect(accounts.length).toBe(3);
                //check for each account individually
                expect(accounts.filter(function (a) { return a.accountId === account1.accountId; }).length).toBe(1);
                expect(accounts.filter(function (a) { return a.accountId === account2.accountId; }).length).toBe(1);
                expect(accounts.filter(function (a) { return a.accountId === account3.accountId; }).length).toBe(1);
                return [2 /*return*/];
        }
    });
}); });
test("Test Get Accounts in Range", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, account1, account2, account3, accounts;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, "account test3");
                return [4 /*yield*/, clientDAO.createClient(client)];
            case 1:
                client = _a.sent();
                //check client was made
                expect(client.clientId).not.toBe(0);
                account1 = new entities_1.Account(0, "Savings", 1000, client.clientId);
                account2 = new entities_1.Account(0, "Savings", 2000, client.clientId);
                account3 = new entities_1.Account(0, "Savings", 3000, client.clientId);
                return [4 /*yield*/, accountDAO.createAccount(account1)];
            case 2:
                account1 = _a.sent();
                return [4 /*yield*/, accountDAO.createAccount(account2)];
            case 3:
                account2 = _a.sent();
                return [4 /*yield*/, accountDAO.createAccount(account3)];
            case 4:
                account3 = _a.sent();
                //check accounts were made
                expect(account1.accountId).not.toBe(0);
                expect(account2.accountId).not.toBe(0);
                expect(account3.accountId).not.toBe(0);
                return [4 /*yield*/, accountDAO.getAccountsInRange(0, 4000)];
            case 5:
                accounts = _a.sent();
                expect(accounts.filter(function (a) { return a.accountId === account1.accountId; }).length).toBe(1);
                expect(accounts.filter(function (a) { return a.accountId === account2.accountId; }).length).toBe(1);
                expect(accounts.filter(function (a) { return a.accountId === account3.accountId; }).length).toBe(1);
                return [4 /*yield*/, accountDAO.getAccountsInRange(1500, 2500)];
            case 6:
                accounts = _a.sent();
                expect(accounts.filter(function (a) { return a.accountId === account1.accountId; }).length).toBe(0);
                expect(accounts.filter(function (a) { return a.accountId === account2.accountId; }).length).toBe(1);
                expect(accounts.filter(function (a) { return a.accountId === account3.accountId; }).length).toBe(0);
                return [2 /*return*/];
        }
    });
}); });
test("Test Get Account By Id", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, account, testAccount;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, "account test4");
                return [4 /*yield*/, clientDAO.createClient(client)];
            case 1:
                client = _a.sent();
                //check client was made correctly
                expect(client.clientId).not.toBe(0);
                account = new entities_1.Account(0, "Savings", 36, client.clientId);
                return [4 /*yield*/, accountDAO.createAccount(account)];
            case 2:
                account = _a.sent();
                //check account was made
                expect(account.accountId).not.toBe(0);
                return [4 /*yield*/, accountDAO.getAccountById(account.accountId)];
            case 3:
                testAccount = _a.sent();
                expect(testAccount.accountId).toBe(account.accountId);
                expect(testAccount.type).toBe(account.type);
                expect(testAccount.amount).toBe(account.amount);
                expect(testAccount.c_id).toBe(account.c_id);
                return [2 /*return*/];
        }
    });
}); });
test("Test Update Account By Id", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, account, testAccount;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, "account test5");
                return [4 /*yield*/, clientDAO.createClient(client)];
            case 1:
                client = _a.sent();
                //check client was made correctly
                expect(client.clientId).not.toBe(0);
                account = new entities_1.Account(0, "Savings", 0, client.clientId);
                return [4 /*yield*/, accountDAO.createAccount(account)];
            case 2:
                account = _a.sent();
                //check account was made
                expect(account.accountId).not.toBe(0);
                account.type = "checking";
                account.amount = 309832;
                return [4 /*yield*/, accountDAO.updateAccount(account)];
            case 3:
                _a.sent();
                return [4 /*yield*/, accountDAO.getAccountById(account.accountId)];
            case 4:
                testAccount = _a.sent();
                expect(testAccount.accountId).toBe(account.accountId);
                expect(testAccount.type).toBe(account.type);
                expect(testAccount.amount).toBe(account.amount);
                expect(testAccount.c_id).toBe(account.c_id);
                return [2 /*return*/];
        }
    });
}); });
test("Test Delete Account By Id", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, account, deleted, accounts, deletedAccount, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, "account test6");
                return [4 /*yield*/, clientDAO.createClient(client)];
            case 1:
                client = _a.sent();
                //check client was made correctly
                expect(client.clientId).not.toBe(0);
                account = new entities_1.Account(0, "Savings", 0, client.clientId);
                return [4 /*yield*/, accountDAO.createAccount(account)];
            case 2:
                account = _a.sent();
                //check account was made
                expect(account.accountId).not.toBe(0);
                return [4 /*yield*/, accountDAO.deleteAccountById(account.accountId)];
            case 3:
                deleted = _a.sent();
                expect(deleted).toBe(true);
                return [4 /*yield*/, accountDAO.getAllClientsAccounts(client.clientId)];
            case 4:
                accounts = _a.sent();
                expect(accounts.filter(function (a) { return a.accountId === account.accountId; }).length).toBe(0);
                _a.label = 5;
            case 5:
                _a.trys.push([5, 7, , 8]);
                return [4 /*yield*/, accountDAO.getAccountById(account.accountId)];
            case 6:
                deletedAccount = _a.sent();
                //line above should throw an error, so this expect should never be reached
                expect(true).toBe(false);
                return [3 /*break*/, 8];
            case 7:
                error_1 = _a.sent();
                expect(error_1.message).toBe("Error: Account with id " + account.accountId + " not found");
                return [3 /*break*/, 8];
            case 8: return [2 /*return*/];
        }
    });
}); });
test("Delete Client with Attached Account", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, account, deleted, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, "account test7");
                return [4 /*yield*/, clientDAO.createClient(client)];
            case 1:
                client = _a.sent();
                //check client was made correctly
                expect(client.clientId).not.toBe(0);
                account = new entities_1.Account(0, "Savings", 36, client.clientId);
                return [4 /*yield*/, accountDAO.createAccount(account)];
            case 2:
                account = _a.sent();
                //check account was made
                expect(account.accountId).not.toBe(0);
                _a.label = 3;
            case 3:
                _a.trys.push([3, 5, , 6]);
                return [4 /*yield*/, clientDAO.deleteClientById(client.clientId)];
            case 4:
                deleted = _a.sent();
                expect(deleted).toBe(false);
                return [3 /*break*/, 6];
            case 5:
                error_2 = _a.sent();
                expect(error_2.message).toBe("Error: Cannot Delete Client " + client.clientId + " as it has Unclosed Accounts");
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}); });
afterAll(function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        connection_1.dbClient.end();
        return [2 /*return*/];
    });
}); });
