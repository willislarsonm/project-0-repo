import express from "express";
import { Account, Client } from "./entities";
import { AccountService } from "./service/account-service";
import { AccountServiceImpl } from "./service/account-service-impl";
import { ClientService } from "./service/client-service";
import { ClientServiceImpl } from "./service/client-service-impl";

const app = express();

const clientService:ClientService = new ClientServiceImpl();
const accountService:AccountService = new AccountServiceImpl();

app.use(express.json());
const port:number = 3000;

app.listen(port, ()=>{
    console.log(`Server started at HTTP://127.0.0.1:${port}`);
});

app.get("/", (req, res) =>{
    res.send("server up")
});

app.post("/clients", async (req, res) => {
    let newClient:Client = req.body;
    newClient = await clientService.registerClient(newClient);
    res.status(201);
    res.send(newClient);
});

app.get("/clients", async (req, res) => {
    try{
        const clients:Client[] = await clientService.retrieveAllClients();
        res.status(200);
        res.send(clients);
    }catch(error){
        res.status(404);
        res.send();
    }
});

app.get("/clients/:clientId", async (req, res) => {
    try{
        const clientId:number = parseInt(req.params.clientId);
        const client:Client = await clientService.retrieveClient(clientId);
        res.status(200);
        res.send(client);
    }catch(error){
        res.status(404);
        res.send(error.message);
    }
});

app.put("/clients/:clientId", async (req, res) => {
    let updateClient:Client = req.body;
    updateClient.clientId = parseInt(req.params.clientId);
    try{
        updateClient = await clientService.updateClient(updateClient);
        res.status(200);
        res.send(updateClient);
    }catch(error){
        res.status(404);
        res.send();
    }
});

app.delete("/clients/:clientId", async (req, res) => {
    const clientId:number = parseInt(req.params.clientId);
    try{
        const deleted:boolean = await clientService.deleteClientById(clientId);
        res.status(205);
        res.send(deleted);
    }catch(error){
        if (error.message === `Error: Client with id ${clientId} not found`){
            //resource not found
            res.status(404);
            res.send(error.message);
        }else{
            //client has attached accounts
            res.status(403);
            res.send(error.message);
        }
    }
});

app.post("/clients/:clientId/accounts", async (req, res) => {
    const clientId:number = parseInt(req.params.clientId);
    try{
        let account:Account = req.body;
        account.c_id = clientId;
        account = await accountService.registerAccount(account);
        res.status(201);
        res.send(account);
    }catch(error){
        res.status(404);
        res.send(error.message);
    }
});

app.get("/clients/:clientId/accounts", async (req, res) => {
    const clientId:number = parseInt(req.params.clientId);
    try{
        //this line is just to make sure that the client exists
        let clientCheck:Client = await clientService.retrieveClient(clientId);
        let accounts:Account[] = await accountService.retrieveClientAccounts(clientId);
        res.status(200);
        res.send(accounts);
    }catch(error){
        res.status(404);
        res.send(error.message);
    }
});

app.get("/accounts", async (req, res) =>{
    if (req.query.amountLessThan && req.query.amountGreaterThan){
        const greaterThan:number = parseInt(req.query.amountGreaterThan as string)
        const lessThan:number = parseInt(req.query.amountLessThan as string);
        let accounts:Account[] = await accountService.retrieveAccountsInRange(greaterThan, lessThan);
        res.status(200);
        res.send(accounts);
    }else{
        res.status(400);
        res.send("Error: amountLessThan and amountGreaterThan must have values")
    }
});

app.get("/accounts/:accoundId", async (req, res) => {
    const accountId:number = parseInt(req.params.accoundId);
    try{
        const account:Account = await accountService.retrieveAccountById(accountId);
        res.status(200);
        res.send(account);
    }catch(error){
        res.status(404);
        res.send(error.message);
    }
});

app.put("/accounts/:accountId", async (req, res) => {
    const accountId:number = parseInt(req.params.accountId);
    let account:Account = req.body;
    account.accountId = accountId;
    try{
        //this line is just to make sure that the client exists
        let clientCheck:Client = await clientService.retrieveClient(account.c_id);
        account = await accountService.updateAccount(account);
        res.status(200);
        res.send(account);
    }catch(error){
        res.status(404);
        res.send(error.message);
    }
});

app.delete("/accounts/:accountId", async (req, res) =>{
    const accountId:number = parseInt(req.params.accountId);
    try{
        const deleted:boolean = await accountService.deleteAccount(accountId);
        res.send(deleted)
    }catch(error){
        res.status(404);
        res.send(error.message);
    }
});

app.patch("/accounts/:accountId/deposit", async (req, res) => {
    const accountId:number = parseInt(req.params.accountId);
    const amount:number = req.body.amount;
    try{
        const updateAccount:Account = await accountService.depositAmount(accountId, amount);
        res.status(200);
        res.send(updateAccount);
    }catch(error){
        if (error.message === "Error: Cannot Deposit a Negative Amount"){
            res.status(422);
        }else{
            res.status(404);
        }
        res.send(error.message);
    }
});

app.patch("/accounts/:accountId/withdraw", async (req, res) => {
    const accountId:number = parseInt(req.params.accountId);
    const amount:number = req.body.amount;
    try{
        const updateAccount:Account = await accountService.withdrawAmount(accountId, amount);
        res.status(200);
        res.send(updateAccount);
    }catch(error){
        if (error.message === "Error: Cannot Withdraw a Negative Amount" ||
            error.message === "Error: Insufficient Funds"){
            res.status(422);
        }else{
            res.status(404);
        }
        res.send(error.message);
    }
});