import { Client } from 'pg';

export const dbClient:Client = new Client({
    user:'postgres',
    password:process.env.DBPASSWORD,
    database:'project0db',
    port:5432,
    host:'35.236.213.14'
});

dbClient.connect();
