import { Client } from "../entities";

export interface ClientDAO{
    //create a client
    createClient(client:Client):Promise<Client>;

    //get all clients
    getAllClients():Promise<Client[]>;

    //get client by Id
    getClientById(clientId:number):Promise<Client>;

    //update client by Id
    updateClient(client:Client):Promise<Client>;

    //delete client by Id
    deleteClientById(clientId:number):Promise<boolean>;
}