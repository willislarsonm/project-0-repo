import { dbClient } from "../connection";
import { Account } from "../entities";
import { ResourceNotFoundError } from "../errors";
import { AccountDAO } from "./account-dao";

export class AccountDAOImpl implements AccountDAO{
    async createAccount(account: Account): Promise<Account> {
        const sql:string = "insert into account (account_type,amount,c_id) values ($1,$2,$3) returning account_id";
        const values:any[] = [account.type, account.amount, account.c_id];
        try{
        const result = await dbClient.query(sql, values);
        account.accountId = result.rows[0].account_id;
        return account;
        }catch(error){
            if (error.message === "insert or update on table \"account\" violates foreign key constraint \"fk_account_client\""){
                throw new ResourceNotFoundError(`Error: Client ${account.c_id} not found`);
            }
        }
    }

    async getAllClientsAccounts(clientId: number): Promise<Account[]> {
        const sql:string = "select * from account where c_id = $1"
        const values:any[] = [clientId];
        const result = await dbClient.query(sql, values);
        return result.rows.map(a => new Account(a.account_id, a.account_type, a.amount, a.c_id));
    }

    async getAccountsInRange(greaterThan: number, lessThan: number): Promise<Account[]> {
        const sql:string = "select * from account where amount < $1 and amount > $2";
        const values:any[] = [lessThan, greaterThan]
        const result = await dbClient.query(sql, values);
        return result.rows.map(a => new Account(a.account_id, a.account_type, a.amount, a.c_id));
    }
    
    async getAccountById(accountId: number): Promise<Account> {
        const sql:string = "select * from account where account_id=$1";
        const values:any[] = [accountId]
        const result = await dbClient.query(sql, values);
        if (result.rowCount === 0){
            throw new ResourceNotFoundError(`Error: Account with id ${accountId} not found`);
        }
        return result.rows.map(a => new Account(a.account_id, a.account_type, a.amount, a.c_id))[0];
    }

    async updateAccount(account: Account): Promise<Account> {
        const sql:string = "update account set account_type=$1, amount=$2, c_id=$3 where account_id=$4"
        const values = [account.type, account.amount, account.c_id, account.accountId];
        const result = await dbClient.query(sql, values);
        if (result.rowCount === 0){
            throw new ResourceNotFoundError(`Error: Account with id ${account.accountId} not found`);
        }
        return account;
    }
    async deleteAccountById(accountId: number): Promise<boolean> {
        const sql:string = "delete from account where account_id = $1";
        const values = [accountId];
        const result = await dbClient.query(sql, values);
        if (result.rowCount == 0){
            throw new ResourceNotFoundError(`Error: Account with id ${accountId} not found`);
        }
        return true;
    }
    
}