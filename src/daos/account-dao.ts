import { Account } from "../entities";

export interface AccountDAO{
    //create account
    createAccount(account:Account):Promise<Account>;

    //get all accounts for a client Id
    getAllClientsAccounts(clientId:number):Promise<Account[]>;

    //get all accounts with value in a range
    getAccountsInRange(greaterThan:number, lessThan:number):Promise<Account[]>;

    //get account by id
    getAccountById(accountId:number):Promise<Account>;

    //update account by id
    updateAccount(account:Account):Promise<Account>;

    //delete account by id
    deleteAccountById(accountId:number):Promise<boolean>;

    
}