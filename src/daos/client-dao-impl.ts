import { dbClient } from "../connection";
import { Client } from "../entities";
import { IllegalDeleteError, ResourceNotFoundError } from "../errors";
import { ClientDAO } from "./client-dao";

export class ClientDAOImpl implements ClientDAO{
    async createClient(client: Client): Promise<Client> {
        const sql:string = "insert into client (client_name) values ($1) returning client_id"
        const values = [client.name];
        const result = await dbClient.query(sql, values);
        client.clientId = result.rows[0].client_id;
        return client;
    }

    async getAllClients(): Promise<Client[]> {
        const sql:string = "select * from client"
        const result = await dbClient.query(sql);
        return result.rows.map(c => new Client(c.client_id, c.client_name));
    }
    async getClientById(clientId: number): Promise<Client> {
        const sql:string = "select * from client where client_id=$1"
        const values = [clientId];
        const result = await dbClient.query(sql, values);
        if (result.rowCount === 0){
            throw new ResourceNotFoundError(`Error: Client with id ${clientId} not found`)
        }
        return result.rows.map(c => new Client(c.client_id, c.client_name))[0];

    }
    async updateClient(client: Client): Promise<Client> {
        const sql:string = 'update client set client_name=$1 where client_id=$2';
        const values:any[] = [client.name, client.clientId];
        const result = await dbClient.query(sql, values);
        if (result.rowCount === 0){
            throw new ResourceNotFoundError(`Error: Client with id ${client.clientId} not found`)
        }
        return client;
    }
    
    async deleteClientById(clientId: number): Promise<boolean> {
        async function checkClientForAccounts(clientId: number):Promise<boolean>{
            const sql:string = "select * from account where c_id = $1"
            const values:any[] = [clientId];
            const result = await dbClient.query(sql, values);
            return result.rowCount > 0;
        }

        const sql:string = 'delete from client where client_id = $1';
        const values:any[] = [clientId];
        try{
            const result = await dbClient.query(sql, values);
            if (result.rowCount === 0){
                throw new ResourceNotFoundError(`Error: Client with id ${clientId} not found`);
            }
            return true;
        }catch(error){
            //this solution isn't great, but works
            if (await checkClientForAccounts(clientId)){
                throw new IllegalDeleteError(`Error: Cannot Delete Client ${clientId} as it has Unclosed Accounts`);
            }else{
                throw new ResourceNotFoundError(`Error: Client with id ${clientId} not found`);
            }
        }
        
    }

}