export class ResourceNotFoundError{
    constructor(
        public message:string
    ){}
}

export class IllegalDeleteError{
    constructor(
        public message:string
    ){}
}

export class InsufficientFundsError{
    constructor(
        public message:string
    ){}
}

export class InvalidInput{
    constructor(
        public message:string
    ){}
}