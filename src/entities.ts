export class Client{
    constructor(
        public clientId:number, 
        public name:string,
    ){}

}

export class Account{
    constructor(
        public accountId:number,
        public type:string,
        public amount:number,
        public c_id:number
    ){}
}