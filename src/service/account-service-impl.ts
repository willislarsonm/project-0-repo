import { AccountDAO } from "../daos/account-dao";
import { AccountDAOImpl } from "../daos/account-dao-impl";
import { Account } from "../entities";
import { InsufficientFundsError, InvalidInput } from "../errors";
import { AccountService } from "./account-service";

const accountDAO:AccountDAO = new AccountDAOImpl();

export class AccountServiceImpl implements AccountService{
    registerAccount(account: Account): Promise<Account> {
        return accountDAO.createAccount(account);
    }
    retrieveClientAccounts(clientId: number): Promise<Account[]> {
        return accountDAO.getAllClientsAccounts(clientId);
    }
    retrieveAccountsInRange(greaterThan: number, lessThan: number): Promise<Account[]> {
        return accountDAO.getAccountsInRange(greaterThan, lessThan);
    }
    retrieveAccountById(accountId: number): Promise<Account> {
        return accountDAO.getAccountById(accountId);
    }
    updateAccount(account: Account): Promise<Account> {
        return accountDAO.updateAccount(account);
    }
    async depositAmount(accountId: number, amount: number): Promise<Account> {
        if (amount < 0){
            throw new InvalidInput("Error: Cannot Deposit a Negative Amount")
        }
        let updateAccount:Account = await accountDAO.getAccountById(accountId);
        updateAccount.amount += amount;
        return accountDAO.updateAccount(updateAccount);
    }
    async withdrawAmount(accountId: number, amount: number): Promise<Account> {
        if (amount < 0){
            throw new InvalidInput("Error: Cannot Withdraw a Negative Amount")
        }
        let updateAccount:Account = await accountDAO.getAccountById(accountId);
        if (updateAccount.amount < amount){
            throw new InsufficientFundsError("Error: Insufficient Funds");
        }
        updateAccount.amount += -amount;
        return accountDAO.updateAccount(updateAccount);
    }
    deleteAccount(accountId:number): Promise<boolean> {
        return accountDAO.deleteAccountById(accountId);
    }
    
}