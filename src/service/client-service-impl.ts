import { ClientDAO } from "../daos/client-dao";
import { ClientDAOImpl } from "../daos/client-dao-impl";
import { Client } from "../entities";
import { ClientService } from "./client-service";

const clientDAO:ClientDAO = new ClientDAOImpl();

export class ClientServiceImpl implements ClientService{
    registerClient(client: Client): Promise<Client> {
        return clientDAO.createClient(client);
    }
    retrieveClient(clientId: number): Promise<Client> {
        return clientDAO.getClientById(clientId);
    }
    retrieveAllClients(): Promise<Client[]> {
        return clientDAO.getAllClients();
    }
    updateClient(client: Client): Promise<Client> {
        return clientDAO.updateClient(client);
    }
    deleteClientById(clientId:number): Promise<boolean> {
        return clientDAO.deleteClientById(clientId);
    }
    
}