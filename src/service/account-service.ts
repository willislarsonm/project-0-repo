import { Client, Account } from "../entities";

export interface AccountService{
    //create
    registerAccount(account:Account):Promise<Account>;

    //read
    retrieveClientAccounts(clientId:number):Promise<Account[]>;
    retrieveAccountsInRange(greaterThan:number, lessThan:number):Promise<Account[]>;
    retrieveAccountById(accountId:number):Promise<Account>;

    //update
    updateAccount(account:Account):Promise<Account>;
    depositAmount(accountId:number, amount:number):Promise<Account>;
    withdrawAmount(accountId:number, amount:number):Promise<Account>;

    //delete
    deleteAccount(accountId:number):Promise<boolean>;
}