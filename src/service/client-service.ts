import { Client } from "../entities";

export interface ClientService{
    //create
    registerClient(client:Client):Promise<Client>;

    //read
    retrieveClient(clientId:number):Promise<Client>;
    retrieveAllClients():Promise<Client[]>;

    //update
    updateClient(client:Client):Promise<Client>;

    //delete
    deleteClientById(clientId:number):Promise<boolean>;
}