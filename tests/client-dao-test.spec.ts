import { dbClient } from "../src/connection";
import { ClientDAO } from "../src/daos/client-dao";
import { ClientDAOImpl } from "../src/daos/client-dao-impl";
import { Client } from "../src/entities";

const clientDAO:ClientDAO = new ClientDAOImpl();

test("Create a client", async ()=>{
    let client:Client = new Client(0, "test1 name");
    client = await clientDAO.createClient(client);
    expect (client.clientId).not.toBe(0);
});

test("Get all clients", async () =>{
    let client1:Client = new Client(0, "test2 name1");
    let client2:Client = new Client(0, "test2 name2");
    let client3:Client = new Client(0, "test2 name3");
    client1 = await clientDAO.createClient(client1);
    client2 = await clientDAO.createClient(client2);
    client3 = await clientDAO.createClient(client3);
    expect (client1.clientId).not.toBe(0);
    expect (client2.clientId).not.toBe(0);
    expect (client3.clientId).not.toBe(0);

    let clients:Client[] = await clientDAO.getAllClients();
    //making sure length increases
    expect (clients.length).toBeGreaterThanOrEqual(3);

    //making sure all 3 clients are returned
    expect (clients.filter(c => c.clientId === client1.clientId).length).toBe(1);
    expect (clients.filter(c => c.clientId === client2.clientId).length).toBe(1);
    expect (clients.filter(c => c.clientId === client3.clientId).length).toBe(1);
});

test("Get client by Id", async () =>{
    let client:Client = new Client(0, "test3 name");
    client = await clientDAO.createClient(client);
    expect (client.clientId).toBeGreaterThan(0);
    let testClient = await clientDAO.getClientById(client.clientId);
    expect(testClient.name).toBe(client.name);
});

test("Update Client", async ()=>{
    let client:Client = new Client(0, "test4 name");
    client = await clientDAO.createClient(client);
    expect (client.clientId).toBeGreaterThan(0);
    client.name = "test4 name update"
    const updatedClient = await clientDAO.updateClient(client);
    expect(updatedClient.name).toBe(client.name);
    expect(updatedClient.clientId).toBe(client.clientId);
});

test("Delete client by Id", async ()=>{
    let client:Client = new Client(0, "test5 name");
    client = await clientDAO.createClient(client);
    expect(client.clientId).not.toBe(0);
    let success = await clientDAO.deleteClientById(client.clientId);
    expect(success).toBe(true);
    try{
        let deletedClient = await clientDAO.getClientById(client.clientId);
        //error should be thrown, this line should NOT run
        expect(0).toBe(1);
    }catch(error){
        expect(error.message).toBe(`Error: Client with id ${client.clientId} not found`);
    }
});

afterAll(async ()=>{
    dbClient.end();
});