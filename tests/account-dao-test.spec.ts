import { dbClient } from "../src/connection";
import { AccountDAO } from "../src/daos/account-dao";
import { AccountDAOImpl } from "../src/daos/account-dao-impl";
import { ClientDAO } from "../src/daos/client-dao";
import { ClientDAOImpl } from "../src/daos/client-dao-impl";
import { Account, Client } from "../src/entities";

const accountDAO:AccountDAO = new AccountDAOImpl();
const clientDAO:ClientDAO = new ClientDAOImpl();

test("Test Create Account", async () =>{
    let client:Client = new Client(0, "account test1");
    client = await clientDAO.createClient(client);
    //check client was made correctly
    expect(client.clientId).not.toBe(0);
    let account:Account = new Account(0, "Savings", 0, client.clientId);
    account = await accountDAO.createAccount(account);
    //check account was made
    expect(account.accountId).not.toBe(0);
});

test("Test Get Client Accounts", async () =>{
    let client:Client = new Client(0, "account test2");
    client = await clientDAO.createClient(client);
    //check client was made
    expect(client.clientId).not.toBe(0);
    let account1:Account = new Account(0, "Savings", 0, client.clientId);
    let account2:Account = new Account(0, "Savings", 0, client.clientId);
    let account3:Account = new Account(0, "Savings", 0, client.clientId);
    account1 = await accountDAO.createAccount(account1);
    account2 = await accountDAO.createAccount(account2);
    account3 = await accountDAO.createAccount(account3);
    //check accounts were made
    expect(account1.accountId).not.toBe(0);
    expect(account2.accountId).not.toBe(0);
    expect(account3.accountId).not.toBe(0);
    const accounts:Account[] = await accountDAO.getAllClientsAccounts(client.clientId);
    //check all 3 were added to the client and no more
    expect(accounts.length).toBe(3);
    //check for each account individually
    expect (accounts.filter(a => a.accountId === account1.accountId).length).toBe(1);
    expect (accounts.filter(a => a.accountId === account2.accountId).length).toBe(1);
    expect (accounts.filter(a => a.accountId === account3.accountId).length).toBe(1);
});

test("Test Get Accounts in Range", async () =>{
    let client:Client = new Client(0, "account test3");
    client = await clientDAO.createClient(client);
    //check client was made
    expect(client.clientId).not.toBe(0);
    let account1:Account = new Account(0, "Savings", 1000, client.clientId);
    let account2:Account = new Account(0, "Savings", 2000, client.clientId);
    let account3:Account = new Account(0, "Savings", 3000, client.clientId);
    account1 = await accountDAO.createAccount(account1);
    account2 = await accountDAO.createAccount(account2);
    account3 = await accountDAO.createAccount(account3);
    //check accounts were made
    expect(account1.accountId).not.toBe(0);
    expect(account2.accountId).not.toBe(0);
    expect(account3.accountId).not.toBe(0);

    //Test to see if accounts are filtered correctly

    let accounts:Account[] = await accountDAO.getAccountsInRange(0, 4000);
    expect (accounts.filter(a => a.accountId === account1.accountId).length).toBe(1);
    expect (accounts.filter(a => a.accountId === account2.accountId).length).toBe(1);
    expect (accounts.filter(a => a.accountId === account3.accountId).length).toBe(1);

    accounts = await accountDAO.getAccountsInRange(1500, 2500);
    expect (accounts.filter(a => a.accountId === account1.accountId).length).toBe(0);
    expect (accounts.filter(a => a.accountId === account2.accountId).length).toBe(1);
    expect (accounts.filter(a => a.accountId === account3.accountId).length).toBe(0);
    
    // TODO clarifiy edge cases
    
    /*
    // Test Edge Cases
    accounts = await accountDAO.getAccountsInRange(1000, 3000);
    expect (accounts.filter(a => a.accountId === account1.accountId).length).toBe(?);
    expect (accounts.filter(a => a.accountId === account2.accountId).length).toBe(1);
    expect (accounts.filter(a => a.accountId === account3.accountId).length).toBe(?);*/
});

test("Test Get Account By Id", async () =>{
    let client:Client = new Client(0, "account test4");
    client = await clientDAO.createClient(client);
    //check client was made correctly
    expect(client.clientId).not.toBe(0);
    let account:Account = new Account(0, "Savings", 36, client.clientId);
    account = await accountDAO.createAccount(account);
    //check account was made
    expect(account.accountId).not.toBe(0);

    const testAccount:Account = await accountDAO.getAccountById(account.accountId);
    expect(testAccount.accountId).toBe(account.accountId);
    expect(testAccount.type).toBe(account.type);
    expect(testAccount.amount).toBe(account.amount);
    expect(testAccount.c_id).toBe(account.c_id);
});

test("Test Update Account By Id", async () =>{
    let client:Client = new Client(0, "account test5");
    client = await clientDAO.createClient(client);
    //check client was made correctly
    expect(client.clientId).not.toBe(0);
    let account:Account = new Account(0, "Savings", 0, client.clientId);
    account = await accountDAO.createAccount(account);
    //check account was made
    expect(account.accountId).not.toBe(0);

    account.type = "checking";
    account.amount = 309832;

    await accountDAO.updateAccount(account)

    const testAccount:Account = await accountDAO.getAccountById(account.accountId);
    expect(testAccount.accountId).toBe(account.accountId);
    expect(testAccount.type).toBe(account.type);
    expect(testAccount.amount).toBe(account.amount);
    expect(testAccount.c_id).toBe(account.c_id);
});

test("Test Delete Account By Id", async () =>{
    let client:Client = new Client(0, "account test6");
    client = await clientDAO.createClient(client);
    //check client was made correctly
    expect(client.clientId).not.toBe(0);
    let account:Account = new Account(0, "Savings", 0, client.clientId);
    account = await accountDAO.createAccount(account);
    //check account was made
    expect(account.accountId).not.toBe(0);

    //testing that accountDAO says it deleted
    const deleted:boolean = await accountDAO.deleteAccountById(account.accountId);
    expect(deleted).toBe(true);

    //testing if account is still with client
    const accounts:Account[] = await accountDAO.getAllClientsAccounts(client.clientId);
    expect (accounts.filter(a => a.accountId === account.accountId).length).toBe(0);

    //testing if account can still be found by id
    try{
        const deletedAccount:Account = await accountDAO.getAccountById(account.accountId);
        //line above should throw an error, so this expect should never be reached
        expect(true).toBe(false);
    }catch(error){
        expect(error.message).toBe(`Error: Account with id ${account.accountId} not found`)
    }
});

test("Delete Client with Attached Account", async() =>{
    let client:Client = new Client(0, "account test7");
    client = await clientDAO.createClient(client);
    //check client was made correctly
    expect(client.clientId).not.toBe(0);
    let account:Account = new Account(0, "Savings", 36, client.clientId);
    account = await accountDAO.createAccount(account);
    //check account was made
    expect(account.accountId).not.toBe(0);
    try{
        //this line should throw an error
        const deleted = await clientDAO.deleteClientById(client.clientId);
        expect(deleted).toBe(false);
    }catch(error){
        expect(error.message).toBe(`Error: Cannot Delete Client ${client.clientId} as it has Unclosed Accounts`);
    }
});

afterAll(async ()=>{
    dbClient.end();
});